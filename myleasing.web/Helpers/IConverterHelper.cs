﻿using System.Threading.Tasks;
using myleasing.web.Data.Entities;
using myleasing.web.Models;

namespace myleasing.web.Helpers
{
    public interface IConverterHelper
    {
        Task<Property> ToPropertyAsync(PropertyViewModel model, bool isNew);
        PropertyViewModel ToPropertyViewMode(Property property);
        Task<Contract> ToContractAsync(ContractViewModel model, bool isNew);
        ContractViewModel ToContractViewModel(Contract contract);
    }
}