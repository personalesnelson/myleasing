﻿using Microsoft.AspNetCore.Http;
using myleasing.web.Data.Entities;
using System.ComponentModel.DataAnnotations;

namespace myleasing.web.Models
{
    public class PropertyImageViewModel : PropertyImage
    {
        [Display(Name = "Image")]
        public IFormFile ImageFile { get; set; }

    }
}
