﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using myleasing.web.Data.Entities;

namespace myleasing.web.Data
{
    //Extends DbContext:
    //Paga crear y administrar nuestra propia seguridad de Usuarios, Roles, Etc.

    //Extends IdentityDbContext<User>:
    //Para Usar la seguridad Generada por Asp.NetCore (IdentityDbContext)

    public class DataContext : IdentityDbContext<User>
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {

        }

        
        public DbSet<Contract> Contracts { get; set; }

        public DbSet<Lessee> Lessees { get; set; }

        public DbSet<Manager> Managers { get; set; }

        public DbSet<Owner> Owners { get; set; }

        public DbSet<Property> Properties { get; set; }

        public DbSet<PropertyImage> PropertyImages { get; set; }

        public DbSet<PropertyType> propertyTypes { get; set; }
        public object PropertyTypes { get; internal set; }
    }
}
