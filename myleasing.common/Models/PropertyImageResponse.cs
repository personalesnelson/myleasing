﻿using System;
using System.Collections.Generic;
using System.Text;

namespace myleasing.common.Models
{
    public class PropertyImageResponse
    {
        public int Id { get; set; }

        public string ImageUrl { get; set; }

    }
}
